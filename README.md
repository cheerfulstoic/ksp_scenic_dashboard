# KspDashboard

This is an Elixir Scenic project which displays a dasboard for Kerbal Space Program.

This was an experimental project to learn about the `kRPC` mod (a way to enable RPC communication with Kerbal Space Program) and about the Scenic library for Elixir.  It was used as a demo for the Stockholm Elixir meetup's project night.  The presentation slides can be found at [this link](https://docs.google.com/presentation/d/1lGbLgxk7om8sme0ElRz00lk8myPNikY8qOJXuifp6yA/edit?usp=sharing)

The [`jooce`](https://github.com/FiniteMonkeys/jooce) project was very useful for getting the `kRPC` connection running, though it needed to be re-built (I think that newer versions of kRSP introduced changes to the protocol).
