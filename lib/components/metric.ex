defmodule KspDashboard.Components.Metric do
  use Scenic.Component

  alias Scenic.Graph
  alias Scenic.ViewPort
  alias Scenic.Sensor

  import Scenic.Primitives
  import Scenic.Components

  def verify(data), do: {:ok, data}

  def init({label, key, font_size, y_offset}, opts) do
    {:ok, %ViewPort.Status{size: {vp_width, vp_height}}} =
      opts[:viewport]
      |> ViewPort.info()

    graph =
      Graph.build(font_size: font_size, translate: {0, y_offset})
      |> rounded_rectangle(
        {vp_width / 2, font_size * 2, 8},
        stroke: {2, :blue},
        fill: :blue,
        translate: {vp_width/ 4, 0}
       )
      |> text(
        "#{label}:",
        text_align: :center,
        font_size: font_size,
        translate: {vp_width / 2, font_size}
      )
      |> text(
        "",
        id: key,
        text_align: :center,
        font_size: font_size,
        translate: {vp_width / 2, font_size * 2}
      )
      |> push_graph()

      Sensor.subscribe(key)

    {:ok, graph}
  end

  # --------------------------------------------------------
  # receive updates from the simulated speed sensor
  def handle_info({:sensor, :data, {key, value, _}}, graph) do
    value = :erlang.float_to_binary(value, decimals: 1)

    if :rand.uniform() < 0.05, do: raise "Whoops"

    graph = graph
    |> Graph.modify(key, &text(&1, value))
    |> push_graph()

    {:noreply, graph}
  end

  def handle_info({:sensor, :registered, id}, graph) do
    {:noreply, graph}
  end

  def handle_info({:sensor, :unregistered, id}, graph) do
    {:noreply, graph}
  end
end

