defmodule KRPC do
  @initial_state %{host: '127.0.0.1', rpc_port: 50000, stream_port: 50001, opts: [], timeout: 5_000}

  alias KRPC.Schema

  def connect(name \\ "Elixir", state \\ @initial_state) do
    {:ok, sock} = :gen_tcp.connect(state.host, state.rpc_port, [:binary, {:active, false}, {:packet, :raw}], state.timeout)

    request =
      Schema.ConnectionRequest.new(client_name: name, type: :RPC) |>
      Schema.ConnectionRequest.encode()

    req_len = request |> String.length |> :gpb.encode_varint
    :ok = :gen_tcp.send(sock, req_len <> request)

    {resp_len, _} = sock |> read_varint |> :gpb.decode_varint
    {:ok, data} = :gen_tcp.recv(sock, resp_len, 3_000)

    %Schema.ConnectionResponse{client_identifier: client_identifier, status: :OK} =
      Schema.ConnectionResponse.decode(data)

    {:ok, {sock, client_identifier}}
  end

  def close(sock) do
    :gen_tcp.close(sock)
  end

  # Valid values for return_type: :double / :float
  def proceduce_call(sock, service, procedure, args \\ [], return_type \\ nil) do
    call = KRPC.Schema.ProcedureCall.new(service: service, procedure: procedure, arguments: build_arguments(args))
    case KRPC.send_request(sock, [call], KRPC.Schema.Response) do
      %KRPC.Schema.Response{error: nil, results: [first_result | _]} ->
        return_value = first_result.value

        case return_type do
          nil -> {:ok, return_value}
          _ ->
            {value, _} = :gpb.decode_type(return_type, return_value, nil)

            {:ok, value}
        end
      %KRPC.Schema.Response{error: error} ->
        {:error, error}
    end
  end

  def build_arguments(args) do
    for {value, i} <- Enum.with_index(args), into: [] do
      KRPC.Schema.Argument.new(position: i, value: value)
    end
  end

  def send_request(sock, calls, response_module) do
    request = Schema.Request.new(calls: calls) |> Schema.Request.encode()

    req_len = request |> String.length |> :gpb.encode_varint
    :ok = :gen_tcp.send(sock, req_len <> request)

    {resp_len, _} = sock |> read_varint |> :gpb.decode_varint
    {:ok, data} = :gen_tcp.recv(sock, resp_len, 3_000)

    response_module.decode(data)
  end

  defp read_varint(sock, buffer \\ <<>>) do
    case :gen_tcp.recv(sock, 1, 3_000) do
      {:ok, <<1 :: size(1), _ :: bitstring>> = byte} ->
        read_varint(sock, buffer <> byte)
      {:ok, <<0 :: size(1), _ :: size(7)>> = byte} ->
        buffer <> byte
      _ ->
        buffer
    end
  end
end
