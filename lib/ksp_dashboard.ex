defmodule KspDashboard do
  @moduledoc """
  Application to display Kerbal Space Program dashboard using scenic
  """

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # load the viewport configuration from config
    main_viewport_config = Application.get_env(:ksp_dashboard, :viewport)

    children = [
      supervisor(KspDashboard.Sensors.Supervisor, []),
      supervisor(Scenic, viewports: [main_viewport_config])
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
