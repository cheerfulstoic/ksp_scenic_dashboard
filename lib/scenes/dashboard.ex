defmodule KspDashboard.Scenes.Dashboard do
  use Scenic.Scene

  alias Scenic.Graph
  alias Scenic.ViewPort
  alias Scenic.Sensor

  alias KspDashboard.Components

  import Scenic.Primitives
  import Scenic.Components

  @body_offset 80
  @font_size 80
  @margin 20
  @button_width 300
  @button_height 100

  # ============================================================================
  # setup

  # --------------------------------------------------------
  def init(_, opts) do
    {:ok, %ViewPort.Status{size: {vp_width, _}}} =
      opts[:viewport]
      |> ViewPort.info()

    col = vp_width / 6

    # build the graph
    graph =
      Graph.build(font: :roboto, font_size: 50, theme: :dark)
      |> Components.Metric.add_to_graph({"Speed", :speed, @font_size, @margin})
      |> Components.Metric.add_to_graph({"Altitude", :altitude, @font_size, (@margin + @font_size) * 2})
      |> button("Stage!",
                id: :btn_stage,
                theme: :danger,
                text_align: :center,
                button_font_size: 50,
                width: @button_width, height: @button_height,
                t: {(vp_width / 2) - (@button_width / 2), ((@margin + @font_size) * 4) + @margin})
      |> push_graph()

    {:ok, sock, control_id} = connect()

    {:ok, %{graph: graph, sock: sock, control_id: control_id}}
  end

  def connect() do
    {:ok, {sock, client_identifier}} = KRPC.connect("Staging")

    {:ok, vessel_id} = KRPC.proceduce_call(sock, 'SpaceCenter', 'get_ActiveVessel')

    {:ok, control_id} = KRPC.proceduce_call(sock, 'SpaceCenter', 'Vessel_get_Control', [vessel_id])

    {:ok, sock, control_id}
  end

  def filter_event({:click, :btn_stage} = event, _, %{graph: graph, sock: sock, control_id: control_id} = state) do
    KRPC.proceduce_call(sock, 'SpaceCenter', 'Control_ActivateNextStage', [control_id])

    {:continue, event, state}
  end
end
