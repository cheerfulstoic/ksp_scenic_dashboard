defmodule KspDashboard.Sensors.FlightMetric do
  use GenServer

  alias Scenic.Sensor

  @version "0.1.0"

  @timer_ms 200

  # --------------------------------------------------------
  def start_link({id, _, _} = data), do: GenServer.start_link(__MODULE__, data, name: :"#{id}_flight_metric")

  # --------------------------------------------------------
  def init({id, procedure, return_type}) do
    Sensor.register(id, @version, "For reading #{procedure} from kRPC")

    state = Map.merge(connect(id), %{id: id, procedure: procedure, return_type: return_type})

    publish(state)

    {:ok, timer} = :timer.send_interval(@timer_ms, :tick)

    {:ok, state}
  end

  def connect(id) do
    {:ok, {sock, client_identifier}} = KRPC.connect("#{id} sensor")

    {:ok, vessel_id} = KRPC.proceduce_call(sock, 'SpaceCenter', 'get_ActiveVessel')

    {:ok, flight_id} = KRPC.proceduce_call(sock, 'SpaceCenter', 'Vessel_Flight', [vessel_id])

    %{sock: sock, client_identifier: client_identifier, flight_id: flight_id}
  end

  def terminate(reason, %{id: id, sock: sock} = state) do
    Sensor.unregister(id)
    KRPC.close(sock)

    :normal
  end

  # --------------------------------------------------------
  def handle_info(:tick, state) do
    if :rand.uniform() < 0.05, do: raise "Whoops"

    publish(state)

    {:noreply, state}
  end

  defp publish(%{sock: sock, flight_id: flight_id, procedure: procedure, return_type: return_type, id: id} = state) do
    {:ok, value} = KRPC.proceduce_call(sock, 'SpaceCenter', procedure, [flight_id], return_type)

    Sensor.publish(id, Float.round(value, 2))
  end
end



