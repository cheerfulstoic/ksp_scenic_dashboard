defmodule KspDashboard.Sensors.Supervisor do
  use Supervisor

  alias KspDashboard.Sensors.FlightMetric

  def start_link() do
    Supervisor.start_link(__MODULE__, :ok)
  end

  def init(:ok) do
    # :observer.start()
    children = [
      Supervisor.child_spec(Scenic.Sensor, []),
      Supervisor.child_spec({FlightMetric, {:altitude, 'Flight_get_SurfaceAltitude', :double}},
        id: {FlightMetric, :altitude}),
      Supervisor.child_spec({FlightMetric, {:speed, 'Flight_get_TrueAirSpeed', :float}},
        id: {FlightMetric, :speed})
    ]

    Supervisor.init(children, strategy: :one_for_one, max_restarts: 20)
  end
end

