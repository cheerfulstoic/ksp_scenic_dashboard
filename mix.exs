defmodule KspDashboard.MixProject do
  use Mix.Project

  def project do
    [
      app: :ksp_dashboard,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {KspDashboard, []},
      extra_applications: [:logger, :connection, :exprotobuf]
      # extra_applications: [:sasl, :logger, :connection, :exprotobuf]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:connection, "~> 1.0.4"},
      {:exprotobuf, "~> 1.2.9"},
      {:scenic, "~> 0.9.0"},
      {:scenic_driver_glfw, "~> 0.9"},
      {:scenic_sensor, "~> 0.7"},
      {:gpb, override: true, git: "git://github.com/CraigCottingham/gpb.git", branch: "export-functions"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end
end
