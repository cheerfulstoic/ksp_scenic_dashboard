

defmodule Foo do
  def metric_loop(sock, flight_id) do
    {:ok, altitude} = KRPC.proceduce_call(sock, 'SpaceCenter', 'Flight_get_SurfaceAltitude', [flight_id], :double)
    IO.puts altitude

    :timer.sleep(100)

    metric_loop(sock, flight_id)
  end
end

{:ok, {sock, client_identifier}} = KRPC.connect()

IO.puts "client_identifier: #{inspect client_identifier}"

call = KRPC.Schema.ProcedureCall.new(service: 'KRPC', procedure: 'GetStatus')
response = KRPC.send_request(sock, [call], KRPC.Schema.Response)

IO.puts inspect(response)
IO.puts inspect(Enum.map(response.results, &KRPC.Schema.Status.decode(&1.value)))




call = KRPC.Schema.ProcedureCall.new(service: 'SpaceCenter', procedure: 'get_ActiveVessel')
response = KRPC.send_request(sock, [call], KRPC.Schema.Response)

IO.puts inspect(response)
vessel_id = List.first(response.results).value
IO.puts "vessel_id: #{inspect vessel_id}"



argument = KRPC.Schema.Argument.new(position: 0, value: vessel_id)
call = KRPC.Schema.ProcedureCall.new(service: 'SpaceCenter', procedure: 'Vessel_get_Control', arguments: [argument])
response = KRPC.send_request(sock, [call], KRPC.Schema.Response)
IO.puts inspect(response)
control_id = List.first(response.results).value
IO.puts "control_id: #{inspect control_id}"



argument = KRPC.Schema.Argument.new(position: 0, value: control_id)
call = KRPC.Schema.ProcedureCall.new(service: 'SpaceCenter', procedure: 'Control_ActivateNextStage', arguments: [argument])
response = KRPC.send_request(sock, [call], KRPC.Schema.Response)
IO.puts inspect(response)


argument = KRPC.Schema.Argument.new(position: 0, value: vessel_id) # to_charlist(control_id))
call = KRPC.Schema.ProcedureCall.new(service: 'SpaceCenter', procedure: 'Vessel_Flight', arguments: [argument])
response = KRPC.send_request(sock, [call], KRPC.Schema.Response)
IO.puts inspect(response)
flight_id = List.first(response.results).value


Foo.metric_loop(sock, flight_id)


